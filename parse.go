//

package main

import (
	//"bufio"
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
)

func main() {
	// Open the file
	csvfile, err := os.Open("input.csv")
	if err != nil {
		log.Fatalln("Couldn't open the csv file", err)
	}

	// Parse the file
	r := csv.NewReader(csvfile)
	//r := csv.NewReader(bufio.NewReader(csvfile))

	// Iterate through the records
	for {
		// Read each record from csv
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		total_gb, err := strconv.ParseFloat(record[4],64)
		est_cost, _ := strconv.ParseFloat(record[5],64)
		fmt.Printf("('%s','%s', %s, %s, %f, %f),\n", record[0], record[1], record[2], record[3], total_gb, est_cost)
	}
}